﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teine_n2dal_vigur
{
    class Program
    {
        static void Main(string[] args)
        {
            Mast m = Mast.Risti;
            m--;
            Console.WriteLine(m);

            Pere t = (Pere)10;
            t |= Pere.Krister;
            Console.WriteLine(t);

            //CASTING tehe
            decimal d = 10;
            int i = (int)(d + 7);
            i = Convert.ToInt16("4f2d", 16);
            Console.WriteLine(i);

            //TÜÜBI TEISENDUS

            //Parse või TryParse
            //Convert klassis on ToType funktsioonid
            //Convert.ToDateTime;


            //SOBIVATE tüüpide puhul castingu tehe
            //(sihttüüp)(lähtetüüpi-avaldis)

            double x = 4.7;
            double y = 0.4;
            int xy = ((int)x + (int)y);
            Console.WriteLine(xy);
            Console.WriteLine((int)x + y);
            Console.WriteLine((int)(x+y));
            Console.WriteLine((int)Math.Round(x+y));
            Console.WriteLine((int)Math.Round(x + y+ 0.4999999999999)); //ümardab alati üles
            Console.WriteLine((int)Math.Ceiling(x + y));//ümardab üles
            Console.WriteLine((int)Math.Floor(x + y));//ümardab alla


        }
    }
    enum Mast {  Ärtu =1, Ruutu, Risti=7, Poti }//KUI EI TAHA ET HAKATAKSE NUMMERDAMA NULLIST, SIIS PEAD ISE ÄRA MÄÄRAMA KUST NUMMERDAMA HAKATAKSE

    [Flags]
    enum Pere { Mart = 1, Thea = 2, Krister = 7 }

   

}
